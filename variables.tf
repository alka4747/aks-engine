variable "prefix" {
  type        = string
  description = "The prefix which should be used for all resources"
  default     = "aks-engine"
}

variable "location" {
  type        = string
  description = "The Azure Region in which all resources should be created."
  default     = "westus2"
}

const fetch = require('node-fetch');
const express = require('express');
const app = express();

var lastBitcoinValues = []

async function GetBitcoinValue() {
    await fetch('https://bitpay.com/api/rates')
        .then(res => res.json())
        .then(json => { for (var i = 0; i < json.length; i++) if (json[i]["code"] == 'USD') lastBitcoinValues.push(json[i]["rate"]) })
}

app.get('/', (req, res) => {
    res.send("Check the logs to know the Bitcoin value")    
});

app.listen(3000, () => setInterval(async function () {
    await GetBitcoinValue();
    console.log("Current Bitcoin value is " + lastBitcoinValues[lastBitcoinValues.length - 1] + "$")
    // Calculate avg value every 10 minutes
    if (lastBitcoinValues.length == 10) {
        var total = 0;
        for (var i = 0; i < lastBitcoinValues.length; i++) {
            total += lastBitcoinValues[i];
        }
        var avg = total / lastBitcoinValues.length;
        console.log("average value in the last 10 minutes is: " + avg + "$");
        lastBitcoinValues = [];
    }
}, 60*1000));


